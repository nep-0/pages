# Jeff's Static Website

This is Jeff's Static Website

## Self introduction

### Basic information

|Key|Value|
|---|---|
|Name|Jeff|
|Nationality|Chinese|
|Gender|Male|
|Birth|2005|
|Profession|Student|

### Technologies

- Programming languages (familiar)
  - `Python 3`
  - `Go 1`
  - `HTML/CSS`
  - `Javascript`
- Programming languages (not so familiar)
  - `SQL`
  - `Kotlin`
  - `Dart`
- Frontend frameworks
  - `Vue`
  - `Flutter`
  - `Electron`
- Backend frameworks
  - `Gin`
  - `Django`

## Projects

### Club Website

|Key|Value|
|---|---|
|Year|2021|
|Languages|`Python` `HTML/CSS` `Javascript`|
|Backend|`Django`|

#### Features
- Login/Sign up
- Inviting reward
- Point system
- Product sale
- Payment

### Smart Classroom

|Key|Value|
|---|---|
|Year|2021|
|Languages|`Go` `Kotlin`|
|Protocol|`WebSocket`|

#### Features
- Login
- Text message sending
- Interaction
- Remote control Slides

### Remote play

|Key|Value|
|---|---|
|Year|2021|
|Languages|`Go` `HTML/CSS`|
|Protocol|`HTTP`|

#### Feature
- Remotely play music

### Ogit

|Key|Value|
|---|---|
|Year|2021|
|Language|`Go`|

#### Feature
- Version control for office files

### Noonsleep

|Key|Value|
|---|---|
|Year|2021|
|Languages|`Go` `Dart`|
|Protocol|`HTTP`|
|Frontend|`Flutter`|
|Backend|`Gin`|

#### Features
- User control
- Student noonsleep information

### Club Union website

|Key|Value|
|---|---|
|Year|2022|
|Languages|`Go` `HTML/CSS` `Javascript`|
|Frontend|`Vue 2`|
|Backend|`Gin`|

#### Features
- User control
- Club selection
- Product sale

### USEC website

|Key|Value|
|---|---|
|Year|2022|
|Languages|`Go` `HTML/CSS` `Javascript`|
|Frontend|`Vue 2`|
|Backend|`Gin`|

#### Features
- User control
- Permission control
- Article control
- Mission control

### Minesweeper

|Key|Value|
|---|---|
|Year|2022|
|Language|`Go`|

#### Feature
- Timing

### Jeff's Account Book

|Key|Value|
|---|---|
|Year|2022|
|Language|`Go`|
|Frontend|`Vue 3`|
|Backend|`Gin`|

#### Features
- Interaction via QQ
- Admin panel
- Point system
- Product sale
- Payment through QQ Wallet
